﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TravelForm
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            int foo = Convert.ToInt32(textBox1.Text);
            int cost = 0;

            if (comboBox1.SelectedIndex == 0 && comboBox2.SelectedIndex == 0) cost = foo * 100;
            if (comboBox1.SelectedIndex == 0 && comboBox2.SelectedIndex == 1) cost = foo * 150;
            if (comboBox1.SelectedIndex == 1 && comboBox2.SelectedIndex == 0) cost = foo * 160;
            if (comboBox1.SelectedIndex == 1 && comboBox2.SelectedIndex == 1) cost = foo * 200;
            if (comboBox1.SelectedIndex == 2 && comboBox2.SelectedIndex == 0) cost = foo * 120;
            if (comboBox1.SelectedIndex == 2 && comboBox2.SelectedIndex == 1) cost = foo * 180;
            if (checkBox1.Checked) cost += 50;
            label4.Text = "Вартість: " + cost + "$.";
        }
    }
}
